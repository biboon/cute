# CUTe

CUTe (for C Unit TEsting) is a single header unit testing framework written in C.

![](example.png)

## Get started

You could just start from the example which I kept very simple. The concepts are very similar to other frameworks :

 * The `startUp` and `tearDown` functions are called before and after each test
 * The `suiteStartUp` and `suiteStartDown` functions are called when before and after running all tests
 * A test is defined by a function from where you just make a bunch of calls to `TEST_ASSERT_*` macros

## Assertions reference

| Assertion macro | Example |
| --- | --- |
| TEST_ASSERT(condition)              | `TEST_ASSERT(1 == 1)`                           |
| TEST_ASSERT_TRUE(condition)         | `TEST_ASSERT_TRUE(1 == 1)`                      |
| TEST_ASSERT_FALSE(condition)        | `TEST_ASSERT_FALSE(2 == 1)`                     |
| TEST_ASSERT_NOT_NULL(ptr)           | `TEST_ASSERT_NOT_NULL(&variable)`               |
| TEST_ASSERT_NULL(ptr)               | `TEST_ASSERT_NULL(NULL)`                        |
| TEST_ASSERT_MEM_EQ(exp, act, len)   | `TEST_ASSERT_MEM_EQ(&var1, &var2, sizeof(var))` |
| TEST_ASSERT_STR_EQ(exp, act)        | `TEST_ASSERT_STR_EQ("aaa", "aaa")`              |
| TEST_ASSERT_PTR_EQ(exp, act)        | `TEST_ASSERT_PTR_EQ(&var, &var)`                |
| TEST_ASSERT_CHAR_OP(op, exp, act)   | `TEST_ASSERT_CHAR_OP(<, 'a', 'b')`              |
| TEST_ASSERT_INT_OP(op, exp, act)    | `TEST_ASSERT_INT_OP(<=, 12, -6)`                |
| TEST_ASSERT_UINT_OP(op, exp, act)   | `TEST_ASSERT_UINT_OP(==, 12, 12)`               |
| TEST_ASSERT_LONG_OP(op, exp, act)   | `TEST_ASSERT_LONG_OP(!=, -6, 12)`               |
| TEST_ASSERT_ULONG_OP(op, exp, act)  | `TEST_ASSERT_ULONG_OP(>, -6, 12)`               |
| TEST_ASSERT_FLOAT_OP(op, exp, act)  | `TEST_ASSERT_FLOAT_OP(!=, 3.1415, 1.618)`       |
| TEST_ASSERT_DOUBLE_OP(op, exp, act) | `TEST_ASSERT_DOUBLE_OP(!=, 3.1415, 1.618)`      |
| TEST_ASSERT_HEX_OP(op, exp, act)    | `TEST_ASSERT_HEX_OP(!=, 1492, 42)`              |

## Other macros

| Macro | Description |
| --- | --- |
| TEST_RUN(func) | Calls the testing function |
| TEST_IGNORE()  | Ignore the calling test and return immediately |
| TEST_BEGIN()   | Must be called at the beginning of the main function |
| TEST_END()     | Must be return at the end of the main function |
