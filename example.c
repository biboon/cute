#include "cute.h"

int x;

void setUp(void)
{
    // Set up test
    x = 42;
}

void tearDown(void)
{
    // Tear down test
}

void suiteSetUp(void)
{
    // Set up test suite
}

void suiteTearDown(void)
{
    // Tear down test suite
}

int isvalid(int y)
{
    return y == 12;
}

void test1(void)
{
    int y = 12;

    TEST_ASSERT(x != y);
    TEST_ASSERT_FALSE(isvalid(x));
    TEST_ASSERT_TRUE(isvalid(y));

    TEST_ASSERT_NOT_NULL(&x);
}

void test2(void)
{
    char a = 'a';
    TEST_ASSERT_CHAR_OP(==, 'b', a);
}

void test3(void)
{
    TEST_IGNORE();

    TEST_ASSERT_INT_OP(==, 1, 1);
}

int main(void)
{
    TEST_BEGIN();

    TEST_RUN(test1);
    TEST_RUN(test2);
    TEST_RUN(test3);

    return TEST_END();
}
