#include "cute.h"


void setUp(void) {}
void tearDown(void) {}
void suiteSetUp(void) {}
void suiteTearDown(void) {}

char aaa[] = "aaa", bbb[] = "bbb";


#define CUTE_TEST_MACROS \
    X(TEST_ASSERT, 1 == 2) \
    X(TEST_ASSERT_TRUE, 1 == 2) \
    X(TEST_ASSERT_FALSE, 1 == 1) \
    X(TEST_ASSERT_NOT_NULL, NULL) \
    X(TEST_ASSERT_NULL, &aaa) \
    X(TEST_ASSERT_MEM_EQ, aaa, bbb, sizeof(aaa)) \
    X(TEST_ASSERT_STR_EQ, aaa, bbb) \
    X(TEST_ASSERT_PTR_EQ, &aaa, &bbb) \
    X(TEST_ASSERT_CHAR_OP, ==, 'a', 'b') \
    X(TEST_ASSERT_INT_OP, ==, 1, 2) \
    X(TEST_ASSERT_UINT_OP, ==, 1U, 2U) \
    X(TEST_ASSERT_LONG_OP, ==, 1L, 2L) \
    X(TEST_ASSERT_ULONG_OP, ==, 1UL, 2UL) \
    X(TEST_ASSERT_FLOAT_OP, ==, 1.618F, 3.14F) \
    X(TEST_ASSERT_DOUBLE_OP, ==, 1.618, 3.14) \
    X(TEST_ASSERT_HEX_OP, ==, 1, 2) \


/* Define test functions */

#define X(macro, ...) \
void cute_test_macro_ ##macro (void) { \
    puts(">>> Testing assertion macro " #macro); \
    macro(__VA_ARGS__); \
    abort(); \
}

CUTE_TEST_MACROS

#undef X


/* Run tests */

#define X(macro, ...) TEST_RUN(cute_test_macro_ ##macro);

int main()
{
    TEST_BEGIN();
    CUTE_TEST_MACROS
    return TEST_END();
}

#undef X
